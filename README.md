# From the Assignment Sheet:

"Eight Puzzle: a sliding puzzle that consists of a frame of numbered square tiles in random order with one tile missing.
In total, there are 9! (362,880) possible states. You will solve a MODIFIED version of the eight puzzle, where the
cost of moving is the value of the tile being moved."

Students are to implement the following searches:
- Breadth-First
- Depth-First
- Uniform Cost
- Best-First
- A*, where the heuristic is the number of out-of-place tiles.
- A*, where the heuristic is the sum of the manhattan distances between all tiles and their correct positions.

# About This Project

In this project, each search is implemented as a separate java class in the "search" package. Representations of
the eight puzzle & other helper classes are in the "lib" package.

