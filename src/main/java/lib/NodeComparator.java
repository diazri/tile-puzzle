package lib;

import java.util.Comparator;

public class NodeComparator implements Comparator<Node> {

	// How we'll compare our nodes.
	private final String comparison;
	private final State goal;
	
	// Different legal comparison methods.
	public static final String COST = "COST";
	public static final String OOP = "OUT-OF-PLACE TILES";
	public static final String MANHATTAN = "MANHATTAN DISTANCES";
	public static final String WMD = "WEIGHTED MANHATTAN DISTANCES";
	
	public static final String OOP_STAR = "OUT-OF-PLACE TILES plus COST";
	public static final String MANHATTAN_STAR = "MANHATTAN DISTANCES plus COST";
	public static final String WMD_STAR = "WEIGHTED MANHATTAN DISTANCES plus COST";
	
	/** A constructor.
	 * 
	 * @param s - the type of comparison we'll perform.
	 */
	public NodeComparator(String s, State g) {
		this.comparison = s;
		this.goal = g;
	}
	
	/** Compare two nodes.
	 * 
	 */
	@Override
	public int compare(Node a, Node b) {
		switch(comparison) {
			// Simple cost comparison.
			case COST: 		return a.getCost() - b.getCost();
			
			// Comparisons based on heuristics alone (for best-first search).
			case OOP:		return a.getH1(goal) - b.getH1(goal);
			case MANHATTAN:	return a.getH2(goal) - b.getH2(goal);
			case WMD:		return a.getH3(goal) - b.getH3(goal);
			
			// Comparisons based on heuristic + cost (for A* search).
			case OOP_STAR:			return ( a.getH1(goal) + a.getCost() ) -  ( b.getH1(goal) + b.getCost() );
			case MANHATTAN_STAR:	return ( a.getH2(goal) + a.getCost() ) -  ( b.getH2(goal) + b.getCost() );
			case WMD_STAR:			return ( a.getH3(goal) + a.getCost() ) -  ( b.getH3(goal) + b.getCost() );
			
			// Default to basic breadth-first search.
			default:		return 1;
		}
	}

}
