package lib;

import java.util.*;
import static lib.Debug.dp;

public class Node {
	
	// Links for building search trees.
	private Node parent;
	private List<Node> children;
	
	// The state that this node represents.
	private final State state;
	
	// Metadata for searches.
	private int cost;
	private int qLength;
	
	/** A constructor.
	 * 
	 * @param s - the state that this node will represent.
	 */
	public Node(State s) {
		this.state = s;
		this.cost = 0;
		this.qLength = 0;
		this.children = new LinkedList<Node>();
	}

	/** Add a child node.
	 * 
	 * @param n - the new child.
	 */
	public void addChild(Node n) {
		n.setParent(this);
		this.children.add(n);
	}
	
	/** Test equality with other nodes.
	 *  True if states are equal, false otherwise.
	 *  We ignore other factors to make life easier.
	 */
	public boolean equals(Object o) {
		if (o.getClass() != this.getClass()) return false;
		else {
			Node n = (Node) o;
			return this.getState().equals(n.getState());
		}
	}
	
	/** A getter for this node's state.
	 * 
	 * @return - the state this node represents.
	 */
	public State getState() {
		return this.state;
	}
	
	/** Compute this node's depth.
	 *  0 at the first level, 1 at the next, and so on.
	 * 
	 * @return - the depth of this node. 
	 */
	public int getDepth() {
		return depth(this.parent);
	}
	
	/** A (recursive) helper method for depth.
	 * 
	 * @param n - the node whose depth we should compute.
	 * @return - the depth of node n.
	 */
	private int depth(Node n) {
		if(n == null) return 0;
		else return 1 + depth(n.parent);
	}
	
	/** A method to compute H1 (num. of out of place tiles).
	 * 
	 * @param goal - the goal state.
	 * @return - H1.
	 */
	public int getH1(State goal) {
		int sum = 0;
		for (int i = 0; i < state.puzzle.length; i++) {
			for (int j = 0; j < state.puzzle.length; j++) {
				if(state.puzzle[i][j] != goal.puzzle[i][j]) sum++;
			}
		}
		return sum;
	}
	
	/** A method to compute H2 (sum of manhattan distances).
	 * 
	 * @param goal - the goal state.
	 * @return - the total of each tile's distance from the goal.
	 */
	public int getH2(State goal) {
		int sum = 0;
		for (int i = 0; i < state.puzzle.length; i++) {
			for (int j = 0; j < state.puzzle.length; j++) {
				sum += manhattanDistance(state.puzzle[i][j], i, j, goal);
			}
		}
		return sum;
	}
	
	/** A method to get our custom heuristic (weighted sum of manhattan distances).
	 * 
	 * @param goal - the goal state.
	 * @return - the sum of the manhattan distances, weighted by each tile's value.
	 */
	public int getH3(State goal) {
		int sum = 0;
		for (int i = 0; i < state.puzzle.length; i++) {
			for (int j = 0; j < state.puzzle.length; j++) {
				int tile = state.puzzle[i][j];
				int wmd = tile * manhattanDistance(tile, i, j, goal);
				sum += wmd;
			}
		}
		return sum;
	}
	
	/** Compute the manhattan distance of a tile from its goal.
	 * 
	 * @param tile - the tile value.
	 * @param x - the tile's x-coordinate.
	 * @param y - the tile's y-coordinate.
	 * @param goal - the goal state.
	 * @return - the manhattan of "tile" from its position on "goal."
	 */
	private int manhattanDistance(int tile, int x, int y, State goal) {
		// Variables for finding tile's location on the goal state.
		int i = 0, j = 0;
		boolean breaker = false;
		
		// Search the goal state for the tile's location.
		for (; i < goal.puzzle.length; i++) {
			for (; j < goal.puzzle.length; j++) {
				if(goal.puzzle[i][j] == tile) breaker = true;
				if(breaker == true) break;
			}
			if(breaker == true) break;
		}
		
		// Calculate the x and y distances.
		int dx = Math.abs(x - i);
		int dy = Math.abs(y - j);
		
		return dx + dy;
	}
	
	/** Get nodes which represent the successors of this node's state.
	 * 
	 * @return - a list of successors.
	 */
	public List<Node> succ() {
		LinkedList<Node> l = new LinkedList<Node>();
		for(State s : state.succ()) {
			Node n = new Node(s);
			n.setCost(cost + state.cost(s));
			l.add(n);
		}
		return l;
	}

	/** Override the hashing function so we can track
	 *  which nodes have been visited/expanded by
	 *  using a hash map. Lazy, but it works.
	 *  
	 *  @return - the hash of this node's state.
	 */
	@Override
	public int hashCode() {
		return state.toString().hashCode();
	}
	
	// From here down, it's just getters and setters for metadata.
	
	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}

	public int getqLength() {
		return qLength;
	}

	public void setqLength(int qLength) {
		this.qLength = qLength;
	}
}
