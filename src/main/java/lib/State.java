package lib;

import java.util.*;

public class State {

	// The puzzle board.
	public final int[][] puzzle;
	
	// The x & y coordinates of the 0 (empty) tile.
	private int x, y;
	
	/** A constructor.
	 */
	public State(int[][] p) {
		// Save the puzzle state.
		this.puzzle = p;
		
		// Find the location of the 0 tile.
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle.length; j++) {
				if(puzzle[i][j] == 0) {
					x = i;
					y = j;
				}
			}
		}
	}
	
	/** Compute the cost to advance to another state.
	 *  This method assumes we've made one, legal move;
	 *  this way, we can just check which tile replaced 0.
	 *  
	 * @param s - the state to advance to.
	 * @return - the cost of the move.
	 */
	public int cost(State s) {
		return s.puzzle[x][y];
	}
	
	/** Compute all legal moves we can make from this state.
	 * 
	 * @return - a list of the states we can reach from this state.
	 */
	public List<State> succ() {
		// Initialize an empty list.
		List<State> l = new LinkedList<State>();
		
		if( up() != null ) l.add( up() );
		if( down() != null ) l.add( down() );
		if( left() != null ) l.add( left() );
		if( right() !=null ) l.add( right() );
		
		// Finally, return our list.
		return l;
	}
	
	/** Compute a legal up move, if any.
	 * 
	 * @return - a State representing the up move (null if none exists).
	 */
	public State down() {
		if(x > 0) {
			return new State( swap( x-1, y, x, y) );
		} else {
			return null;
		}
	}
	
	/** Compute a legal down move, if any.
	 * 
	 * @return - a State representing the up move (null if none exists).
	 */
	public State up() {
		if(x < 2) {
			return new State( swap( x+1, y, x, y) );
		} else {
			return null;
		}
	}
	
	/** Compute a legal down move, if any.
	 * 
	 * @return - a State representing the up move (null if none exists).
	 */
	public State right() {
		if(y > 0) {
			return new State( swap( x, y-1, x, y) );
		} else {
			return null;
		}
	}
	
	/** Compute a legal down move, if any.
	 * 
	 * @return - a State representing the up move (null if none exists).
	 */
	public State left() {
		if(y < 2) {
			return new State( swap( x, y+1, x, y) );
		} else {
			return null;
		}
	}
	
	/** Compute a copy of our array, with values (a,b) and (c,d) swapped. 
	 * 
	 * @param a - the x-coordinate of the first value to swap.
	 * @param b - the y-coordinate of the first value to swap.
	 * @param c - the x-coordinate of the second value to swap.
	 * @param d - the y-coordinate of the second value to swap.
	 * @return - the array, post-swap.
	 */
	public int[][] swap(int a, int b, int c, int d) {
		// Initialize a new array and copy all the values from our puzzle.
		int[][] out = new int[puzzle.length][puzzle.length];
		for (int i = 0; i < puzzle.length; i++) {
			for (int j = 0; j < puzzle.length; j++) {
				out[i][j] = puzzle[i][j];
			}
		}
		
		// Conduct the swap.
		int temp = out[a][b];
		out[a][b] = out [c][d];
		out[c][d] = temp;
		
		return out;
	}
	
	/** A method to test equality.
	 */
	@Override
	public boolean equals(Object o) {
		if(o.getClass() != this.getClass()) return false;
		else {
			State s = (State) o;
			if(s.puzzle.length != this.puzzle.length) return false;
			for (int i = 0; i < puzzle.length; i++) {
				for (int j = 0; j < puzzle.length; j++) {
					if(s.puzzle[i][j] != this.puzzle[i][j]) return false;
				}
			}
			return true;
		}
	}
	
	/** A method so java will print states in a pretty way.
	 */
	public String toString() {
		String out = "-------------";
		
		for (int i = 0; i < puzzle.length; i++) {
			out += "\n| ";
			
			for (int j = 0; j < puzzle.length; j++) {
				out += puzzle[i][j] + " | ";
			}
			
			out += "\n-------------";
		}
		
		return out;
	}
}
