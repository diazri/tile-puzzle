package search;

import java.util.*;
import lib.*;
import static lib.Debug.dp;

public abstract class Search {
	
	// A queue for our "pretty print" method.
	private static LinkedList<Node> printQ;
	
	// A length counter, also for pp.
	private static int len;
	
	/** Run a search from state s to state g.
	 * 
	 * @param s - the starting state.
	 * @param g - the goal state.
	 * @return - the node which matches the goal.
	 */
	public abstract Node search(State s, State g);
	
	/** "Pretty print" the path from n's oldest ancestor
	 *  (i.e. the start state) to n, with metadata.
	 *  
	 * @param n - the leaf node to start printing from.
	 */
	public static void pp(Node n) {
		printQ = new LinkedList<Node>();
		len = 0;
		
		Node i = n;
		
		// Put nodes on the printing queue and compute the length of the path.
		while(i != null) {
			printQ.addFirst(i);
			len++;
			if(i.getParent() != null) {
				i = i.getParent();
			} else {
				i = null;
			}
		}
		
		// Print everything we put on the print queue.
		for(Node x : printQ) {
			System.out.println("Next move: ");
			System.out.println(x.getState());
		}
		
		// Print the metadata.
		System.out.println();
		System.out.println("Length of solution path: " + len);
		System.out.println("Cost of solution: " + n.getCost());
		System.out.println("Maximum size of queue: " + n.getqLength());
	}
	
	/** Take the max of two ints.
	 * 
	 * @param i - the first int.
	 * @param j - the second int.
	 * @return - the max.
	 */
	private static int max(int i, int j) {
		if(i > j) return i;
		else return j;
	}
}
