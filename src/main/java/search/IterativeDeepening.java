package search;

import static lib.Debug.dp;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import lib.Node;
import lib.State;

public class IterativeDeepening extends Search {

	// Nodes which represent our start and goal states.
	private Node start;
	private Node goal;

	// Our queue.
	private LinkedList<Node> q;

	// The maximum length of the queue.
	private int mlq;

	// A hashmap to track what's been visited/expanded.
	// No entry for unvisited. False for visited but
	// unexpanded. True for visited and expanded.
	private HashMap<Node, Boolean> map;

	/**
	 * Do Iterative Deepening Search to reach state g from state s.
	 * 
	 * @param s - the starting state.
	 * @param g - the ending state.
	 */
	public Node search(State s, State g) {
		dp("Initializing search . . .");

		// Save our start and goal states.
		this.start = new Node(s);
		this.goal = new Node(g);

		// The depth which we'll search up to.
		int cutoff = 100000;
		
		dp("Entering iterative loop . . .");

		// Loop to increase depth of search . . .
		for(int d = 0; d < cutoff; d++) {
			dp("Entering deepening loop, d = " + d +" . . .");
			
			// Initialize our map & queue.
			map = new HashMap<Node, Boolean>();
			q = new LinkedList<Node>();
			q.addLast(start);
			
			// Loop for DFS . . .
			Node n;
			while (!q.isEmpty()) {
				// Pop and test to see if we've reached our goal.
				n = q.removeFirst();
				map.put(n, true);
				dp("Popped node:\n" + n.getState().toString());
				if (n.equals(goal)) {
					dp("Reached goal!");
					n.setqLength(mlq);
					return n;
				}
				// If we need to keep searching,
				else {
					List<Node> succs = n.succ();
					dp("Number of successors: " + succs.size());
					// Expand this node's successors,
					for (Node m : succs) {
						dp("Testing successor:\n" + m.getState().toString());
						// If the successor is unvisited,
						if (map.get(m) == null) {
							dp("Successor is unvisited. Marking & checking depth . . .");

							// Make sure we know the max queue length.
							dp("Queue length is: " + q.size());
							if (q.size() > mlq)
								mlq = q.size();

							// Add unvisited successor to n's children and the visited/expanded map.
							n.addChild(m);
							map.put(m, false);
							
							// If the node is less than the alloted depth, add it to the BEGINNING of the queue.
							if(m.getDepth() <= d) {
								dp("Successor is at a legal depth. Adding to queue . . .");
								q.addFirst(m);
							}
						} else {
							dp("Successor has been visited. Skipping . . .");
						}
					}
					// Also, mark this node as expanded.
					map.put(n, true);
				}
			}
		}
		return null;
	}

}
