package search;

import static lib.Debug.dp;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.PriorityQueue;

import lib.Node;
import lib.NodeComparator;
import lib.State;

public class BestFirst extends Search {

	// Nodes which represent our start and goal states.
	private Node start;
	private Node goal;

	// The heuristic our search will use.
	private final String heuristic;

	// A comparator to use when we build our queue.
	private NodeComparator comp;

	// Our queue (Java's implementation of a minheap).
	private PriorityQueue<Node> q;

	// The maximum length of the queue.
	private int mlq;

	// A hashmap to track what's been visited/expanded.
	// No entry for unvisited. False for visited but
	// unexpanded. True for visited and expanded.
	private HashMap<Node, Boolean> map;

	/**
	 * A constructor.
	 * 
	 * @param h - the heuristic this search will use.
	 */
	public BestFirst(String h) {
		this.heuristic = h;
	}

	@Override
	public Node search(State s, State g) {
		dp("Initializing search . . .");

		// Save our start and goal states.
		this.start = new Node(s);
		this.goal = new Node(g);

		// Initialize our map & queue.
		map = new HashMap<Node, Boolean>();
		Comparator<Node> c = new NodeComparator(heuristic, g); // Compare nodes by their cost.
		q = new PriorityQueue<Node>(c);
		q.add(start);

		dp("Entering loop . . .");

		// Loop for UCS . . .
		Node n;
		while (!q.isEmpty()) {
			// Pop and test to see if we've reached our goal.
			n = q.poll();
			map.put(n, true);
			dp("Popped node:\n" + n.getState().toString());
			if (n.equals(goal)) {
				dp("Reached goal!");
				n.setqLength(mlq);
				return n;
			}
			// If we need to keep searching,
			else {
				List<Node> succs = n.succ();
				dp("Number of successors: " + succs.size());
				// Expand this node's successors,
				for (Node m : succs) {
					dp("Testing successor:\n" + m.getState().toString());
					// If the successor is unvisited,
					if (map.get(m) == null) {
						dp("Successor is unvisited. Adding to queue . . .");

						// Make sure we know the max queue length.
						dp("Queue length is: " + q.size());
						if (q.size() > mlq)
							mlq = q.size();

						// Add unvisited successor to n's children, the UCS queue, and the
						// visited/expanded map.
						n.addChild(m);
						q.add(m);
						map.put(m, false);
					} else {
						dp("Successor has been visited. Skipping . . .");
					}
				}
				// Also, mark this node as expanded.
				map.put(n, true);
			}
		}
		return null;
	}

}
