package main;

import lib.*;
import search.*;

import org.apache.commons.cli.*;

import static lib.Debug.dp;

// The class that runs all our searches.
public class Main {

	// Our search object.
	private static Search search;
	
	// The goal state.
	private static final int[][] goal = { {1, 2, 3}, {8, 0, 4}, {7, 6, 5} };
	
	// The initial states we were assigned in class.
	private static final int[][] easy = { {1, 3, 4}, {8, 6, 2}, {7, 0, 5} };
	private static final int[][] med = { {2, 8, 1}, {0, 4, 3}, {7, 6, 5} };
	private static final int[][] hard = { {5, 6, 7}, {4, 0, 8}, {3, 2, 1} };
	
	// Corresponding strings for each difficulty (used in parsing command line args).
	private static final String EASY = "easy";
	private static final String MEDIUM = "medium";
	private static final String HARD = "hard";
	
	// Some search types (used in parsing command line args).
	private static final String BFS = "bfs";
	private static final String DFS = "dfs";
	private static final String UCS = "ucs";
	private static final String ITR = "itr";
	private static final String GBF = "gbf";
	private static final String AStar = "a*";
	
	/** The "main" method for running searches.
	 * 
	 * @param args - a 3-character code for a search, and optional flags . . .
	 * 
	 *  -s (for picking a search)
	 *  -d (for picking a difficulty setting)
	 * 	-c (for picking a comparator)
	 *  -h (for displaying the help menu)
	 */
	public static void main(String[] args) {
		
		// Our commandline object for handling arguments.
		CommandLine cl;
		
		// Define command line options
		Options ops = new Options();
		
		Option opt = Option.builder("s")
				.longOpt("search")
				.hasArg()
				.desc("Choose which search (bfs, dfs, ucs, itr, gbf, a*) to run."
						   + "\n\tbfs = Breadth-First Search"
						   + "\n\tdfs = Depth-First Search"
						   + "\n\tucs = Uniform-Cost Search"
						   + "\n\titr = Iterative Deepening"
						   + "\n\tgbf = Greedy Best-First Search"
						   + "\n\ta* = A* (A Star) Search")
				.build();
		ops.addOption(opt);
		
		opt = Option.builder("c")
				.longOpt("comparator")
				.hasArg()
				.desc("Select the comparator (cost, oop, manhattan, wmd, oop_star, manhattan_star, wmd_star) to use for greedy best-first or A* search.")
				.build();
		ops.addOption(opt);
		
		opt = Option.builder("d")
				.longOpt("difficulty")
				.hasArg()
				.desc("Select the difficulty level (easy, medium, or hard) of puzzle to solve.")
				.build();
		ops.addOption(opt);
		
		ops.addOption("h", "help", false, "Show the help menu.");
		
		// Parse the command line options
		CommandLineParser p = new DefaultParser();
		try {
			cl = p.parse(ops, args);
		} catch (ParseException e) {
			System.out.println("ERROR: Invalid arguments.");
			e.printStackTrace();
			return;
		}
		
		System.out.println("Initializing states . . .");
		
		// Defaults.
		State s = new State(easy);
		State g = new State(goal);
		String h = NodeComparator.WMD_STAR;
		search = new BFS();
		
		// If the user wants to see the help menu, show them and end the program.
		if(cl.hasOption("h")) {
			HelpFormatter help = new HelpFormatter();
			help.printHelp("SearchApp", ops);
			return;
		}
		
		// If the user specifies a difficulty, use it.
		if(cl.hasOption("d")) {
			switch(cl.getOptionValue("d")) {
				case EASY: 		s = new State(easy); break;
				case MEDIUM:	s = new State(med); break;
				case HARD: 		s = new State(hard); break;
				default: 		//Do nothing
			}
		}
		
		// If the user specifies a comparator, use it.
		if(cl.hasOption("c")) {
			switch(cl.getOptionValue("c")) {
				case NodeComparator.COST: 			h = NodeComparator.COST; break;
				case NodeComparator.MANHATTAN:	 	h = NodeComparator.MANHATTAN; break;
				case NodeComparator.MANHATTAN_STAR: h = NodeComparator.MANHATTAN_STAR; break;
				case NodeComparator.OOP: 			h = NodeComparator.COST; break;
				case NodeComparator.OOP_STAR: 		h = NodeComparator.COST; break;
				case NodeComparator.WMD: 			h = NodeComparator.COST; break;
				case NodeComparator.WMD_STAR: 		h = NodeComparator.COST; break;
				default: 							//Do nothing
			}
		}
		
		// If the user specifies a search, use it.
		if(cl.hasOption("s")) {
			switch(cl.getOptionValue("s")) {
				case BFS: 	search = new BFS(); break;
				case DFS: 	search = new DFS(); break;
				case UCS: 	search = new UniformCost(); break;
				case ITR: 	search = new IterativeDeepening(); break;
				case GBF: 	search = new BestFirst(h); break;
				case AStar: search = new AStar(h); break;
				default: 	//Do nothing
			}
		}
		
		System.out.println("Running search: " + search.getClass().getName() + " . . .");
		
		long t0 = System.currentTimeMillis();
		
		Node result = search.search(s, g);
		
		long t1 = System.currentTimeMillis();
		
		System.out.println("Finished search! Printing path . . .");
		search.pp(result);
		System.out.println("Runtime of search (in milliseconds): " + (t1 - t0) );

	}

}
