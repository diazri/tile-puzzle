package libtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.*;
import org.junit.*;
import lib.State;

public class StateTest {

	// All moves are legal.
	int[][] all = { { 1, 2, 3 }, { 4, 0, 5 }, { 6, 7, 8 } };
	
	// All moves but up are legal.
	int[][] ez = { {1, 3, 4}, {8, 6, 2}, {7, 0, 5} };

	// Only down and right are legal.
	int[][] dr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 0 } };

	// Only up and left are legal.
	int[][] ul = { { 0, 1, 2 }, { 3, 4, 5 }, { 6, 7, 8 } };

	// The result of moving up, down, left, and right from the "all" state.
	int[][] resUp = { { 1, 2, 3 }, { 4, 7, 5 }, { 6, 0, 8 } };
	int[][] resDown = { { 1, 0, 3 }, { 4, 2, 5 }, { 6, 7, 8 } };
	int[][] resLeft = { { 1, 2, 3 }, { 4, 5, 0 }, { 6, 7, 8 } };
	int[][] resRight = { { 1, 2, 3 }, { 0, 4, 5 }, { 6, 7, 8 } };

	// The states we'll use for testing the up, down, left, and right functions.
	State allState, allState2, ezState, drState, ulState, resState;
	
	// More states for testing the successor function.
	State upState, downState, leftState, rightState;

	// Prep everything for testing.
	@Before
	public void setup() {
		allState = new State(all);
		allState2 = new State(all);
		ezState = new State(ez);
		drState = new State(dr);
		ulState = new State(ul);
		
		upState = new State(resUp);
		downState = new State(resDown);
		leftState = new State(resLeft);
		rightState = new State (resRight);
	}

	// Test the equality function.
	@Test
	public void testEquals() {
		assertTrue(allState.equals(allState2));
		assertTrue(allState2.equals(allState));
		assertFalse(allState.equals(drState));
	}

	// Test the swap function.
	@Test
	public void testSwap() {
		int[][] up = allState.swap(1, 1, 2, 1);
		assertEquals(up, resUp);
	}

	// Test the up function.
	@Test
	public void testUp() {
		resState = new State(resUp);
		assertEquals(allState.up(), resState);
		assertNull(drState.up());
		assertNotNull(ulState.up());
	}

	// Test the down function.
	@Test
	public void testDown() {
		resState = new State(resDown);
		assertEquals(allState.down(), resState);
		assertNull(ulState.down());
		assertNotNull(drState.down());
	}

	// Test the left function.
	@Test
	public void testLeft() {
		resState = new State(resLeft);
		assertEquals(allState.left(), resState);
		assertNotNull(ulState.left());
		assertNull(drState.left());
	}

	// Test the left function.
	@Test
	public void testRight() {
		resState = new State(resRight);
		assertEquals(allState.right(), resState);
		assertNotNull(drState.right());
		assertNull(ulState.right());
	}
	
	// Test the successor function.
	@Test
	public void testSucc() {
		List<State> succs = allState.succ();
		assertTrue(succs.contains(upState));
		assertTrue(succs.contains(downState));
		assertTrue(succs.contains(leftState));
		assertTrue(succs.contains(rightState));
		assertEquals(4, succs.size());
		
		succs = ezState.succ();
		assertEquals(3, succs.size());
		
		succs = ulState.succ();
		assertEquals(2, succs.size());
		
		succs = drState.succ();
		assertEquals(2, succs.size());
	}
	
	// Test the cost function.
	@Test
	public void testCost() {
		assertEquals(allState.cost(upState), 7);
		assertEquals(allState.cost(downState), 2);
		assertEquals(allState.cost(leftState), 5);
		assertEquals(allState.cost(rightState), 4);
	}
}
